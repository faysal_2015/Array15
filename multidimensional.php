<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        
        <?php
        echo "<pre>";
       echo "<h4>Example 1: </h4>"; 
       $a = array(10,10.2,array("pavel","parvej"),"pavel_parvej");
       print_r($a)."<br>";
       
       echo "<h4>Example 2: </h4>"; 
       $b = array(array("pavel","parvej"),10,20.34,array("basis","bitm"));
       print_r($b)."<br>";
       
       echo "<h4>Example 3: </h4>"; 
       $c = array(array(array(array("html","css","php"))));
       print_r($c)."<br>";
       
       echo "<h4>Example 4: </h4>"; 
       $d = array(array(10,20.20),array("pavel"),array("html","css","php"));
       print_r($d)."<br>";
       
       echo "<h4>Example 5: </h4>"; 
       $e = array(array(10,20.20,array("pavel",array("html","css","php"))));
       print_r($e)."<br>";
       echo "</pre>";
        ?>
    </body>
</html>